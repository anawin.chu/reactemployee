
import Axios from 'axios';

Axios.defaults.timeout = 1000 * 60 * 3;
Axios.defaults.headers.common['Content-Type'] = 'application/json';





function post(url, body) {
  return this.axios.post(url, body).finally(() => {
    this.callback();
  });
}

function put(url, body) {
  return this.axios.put(url, body).finally(() => {
    this.callback();
  });
}

export async function removeEmployee(id) {
  return await Axios.delete('http://localhost:8080/deleteEmployee/'+id)   
 
}

export async function addEmployee(employees) {
     return await Axios.post('http://localhost:8080/addEmployee', employees);   
}

export async function editEmployee(employees) {
  return await Axios.post('http://localhost:8080/updateEmployee', employees);   
}

export async function getEmployee() {
  return await Axios.get('http://localhost:8080/getAllEmployees')   
}

export async function getEmployeeById(id) {
  return await Axios.get('http://localhost:8080/getEmployee/'+id)   
}
