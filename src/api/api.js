import Axios from 'axios'; 

Axios.defaults.timeout = 1000 * 60 * 3;
Axios.defaults.headers.common['Content-Type'] = 'application/json';

export const axios = Axios.create({
  baseURL: 'http://localhost:8080',
});

class UseAPI {
 

  get(url) {
 
    return this.axios
      .get(url )
      .finally(() => {
        this.callback();
      });
  }

  post(url, body ) {
 
    return this.axios
      .post(url, body )
      .finally(() => {
        this.callback();
      });
  }

  put(url, body ) {
   
    return this.axios
      .put(url, body )
      .finally(() => {
        this.callback();
      });
  }

 
}
 

export default useAPI;
