import React, { Fragment, useState, useContext, useEffect } from 'react';
import { editEmployee ,getEmployeeById } from '../context/GlobalState';
import { useHistory, Link } from "react-router-dom";

export const Editemployee = (route) => {
    let history = useHistory();
    
    const [id, setId] = useState(''); 
    const [employeeId, setEmployeeId] = useState(''); 
    
    const [firstName, setFirstName] = useState(''); 
    const [lastName, setLastName] = useState(''); 
    const [gender, setGender] = useState(''); 
    const [position, setPosition] = useState(''); 
    const [salary, setSalary] = useState(0); 

    const currentId = route.match.params.id;

    useEffect(() => {
  

        const fetchData = async () => {
            const employeeId = currentId;
            let employees  = await getEmployeeById(employeeId);
            console.log('Edit Employee')
            console.log(employees)
            setEmployeeId( employees.data.employeeId)
            setFirstName(employees.data.firstName)
            setGender (employees.data.gender)
            setId(employees.data.id)
            setLastName (employees.data.lastName)
            setPosition (employees.data.position)
            setSalary (employees.data.salary)
          };
     
          fetchData();
        // eslint-disable-next-line
    }, []);

    const onSubmit = async e => {
        e.preventDefault();

        const employee = {  
            id:id,
            employeeId : employeeId,        
            firstName: firstName,
            lastName: lastName,
            gender: gender,
            position: position,
            salary:salary
          };
        
        await editEmployee(employee);
        history.push('/');
    }

    

    return (
        <Fragment>
             <div className="w-full max-w-sm container mt-20 mx-auto">
                <form onSubmit={onSubmit}>
                    <div className="w-full mb-5">
                        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="firstName">
                            First Name
                        </label>
                        <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:text-gray-600" value={firstName} onChange={(e) => setFirstName(e.target.value)} type="text" placeholder="Enter firstName" />
                    </div>
                    <div className="w-full mb-5">
                        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="lastName">
                            Last Name
                        </label>
                        <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:text-gray-600" value={lastName} onChange={(e) => setLastName(e.target.value)} type="text" placeholder="Enter lastName" />
                    </div>

                
                    <div className="w-full  mb-5">
                        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="gender">
                            Gender
                        </label>
                        <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:text-gray-600 focus:shadow-outline" value={gender} onChange={(e) => setGender(e.target.value)} type="text" placeholder="Enter Gender" />
                    </div>
                    <div className="w-full  mb-5">
                        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="position">
                            Position
                        </label>
                        <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:text-gray-600" value={position} onChange={(e) => setPosition(e.target.value)} type="text" placeholder="Enter Position" />
                    </div>

                    <div className="w-full  mb-5">
                        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="salary">
                            Salary
                        </label>
                        <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:text-gray-600" value={salary} onChange={(e) => setSalary(e.target.value)} type="text" placeholder="Enter Salary" />
                    </div>
                    <div className="flex items-center justify-between">
                        <button className="mt-5 bg-green-400 w-full hover:bg-green-500 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
                            Edit Employee
                        </button>
                    </div>
                    <div className="text-center mt-4 text-gray-500"><Link to='/'>Cancel</Link></div>
                </form>
            </div>
        </Fragment>
    )
}